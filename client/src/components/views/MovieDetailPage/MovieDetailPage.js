import React, { useEffect, useState } from "react";
import { API_KEY, API_URL, IMAGE_URL } from "../../Config";
import MainImage from "../LandingPage/Sections/MainImage";
import { Descriptions, Button, Row } from "antd";
import GridCard from "../LandingPage/Sections/GridCard";
import Favorite from "./Sections/Favorite";
import Comments from './Sections/Comments'
import axios from 'axios';
import LikeDislikes from "./Sections/LikeDislikes";


function MovieDetailPage(props) {

  const movieId = props.match.params.movieId;

  const [Movie, setMovie] = useState([]);
  const [Crews, setCrews] = useState([]);
  const [ActorToggle, setActorToggle] = useState(false);
  const [CommentLists, setCommentLists] = useState([]);



  useEffect(() => {
    const movieVariable = {
      movieId: movieId
    }
    // match.params permet de récupérer des parametres de l'URL
    // Defini dans le fichier app.js

    fetch(`${API_URL}movie/${movieId}?api_key=${API_KEY}&language=en-US`)
      .then((response) => response.json())
      .then((response) => {
        console.log(response);
        setMovie(response);

        fetch(`${API_URL}movie/${movieId}/credits?api_key=${API_KEY}`)
          .then(response=> response.json())
          .then(response =>{
            console.log(response)
            setCrews(response.cast)
          })
      });

      axios.post('/api/comment/getComments', movieVariable)
        .then(response =>{
          if(response.data.success){
            console.log(response.comment)
            setCommentLists(response.data.comments)
          }else{

          }
        })




  }, [movieId]);

  const handleClick = () =>{
    setActorToggle(!ActorToggle);
  }

const updateComment = (commentData, action) => {
    if (action === "delete") {
        const deletedCommentId = commentData;
        setCommentLists(CommentLists.filter(comment => comment._id !== deletedCommentId));
    } else if (action === "update") {
        const updatedComments = [...CommentLists]; // Crée une copie de la liste existante
        const index = updatedComments.findIndex(comment => comment._id === commentData._id); // Trouve l'index de l'élément à mettre à jour
        if (index !== -1) { // Vérifie si l'élément a été trouvé
            updatedComments[index] = { ...updatedComments[index], ...commentData }; // Met à jour l'élément spécifique avec les nouvelles données
            console.log("avant", CommentLists);
            setCommentLists(updatedComments); // Met à jour l'état avec la liste mise à jour
            console.log("apres", updatedComments);
        }
    } else {
        setCommentLists(CommentLists.concat(commentData));
    }
};






  return (
    <div>
      {Movie && (
        <MainImage
          image={`${IMAGE_URL}w1280${
            Movie.backdrop_path && Movie.backdrop_path
          }`}
          title={Movie.original_title}
          text={Movie.overview}
        />
      )}

      {/* ---------- Body -------- */}
      <div style={{ width: "85%", margin: "1rem auto" }}>

      <div style={{ display: "flex", justifyContent: "flex-end" }}>
        <div style={{ marginRight: "10px" }}> 
          <LikeDislikes movie movieId={movieId} userId={localStorage.getItem('userId')}/> 
        </div>
        <Favorite userFrom={localStorage.getItem('userId')} movieId={movieId} movieInfo={Movie} /> {/* Composant Favorite */}
      </div>



        {/* Movie Info Table */}
        <Descriptions title="Movie Info" bordered>
          <Descriptions.Item label="Title">{Movie.original_title}</Descriptions.Item>
          <Descriptions.Item label="release_date">{Movie.release_date}</Descriptions.Item>
          <Descriptions.Item label="revenue">{Movie.revenue}</Descriptions.Item>
          <Descriptions.Item label="runtime">{Movie.runtime}</Descriptions.Item>
          <Descriptions.Item label="vote_average">{Movie.vote_average}</Descriptions.Item>
          <Descriptions.Item label="vote_count">{Movie.vote_count}</Descriptions.Item>
          <Descriptions.Item label="status">{Movie.status}</Descriptions.Item>
          <Descriptions.Item label="popularity">{Movie.popularity}</Descriptions.Item>
        </Descriptions>

        <br/>
        <br/>

        <div style={{ display: "flex", justifyContent: "center" }}>
          <Button onClick={handleClick}> Toggle Actor View</Button>
        </div>

        {/* Grid Cars for Crews */}

        {ActorToggle &&         
        <Row gutter={[16, 16]}>
          {Crews &&
            Crews.map((crew, index) => (
              <React.Fragment key={index}>

                {crew.profile_path &&
                  <GridCard
                  actor image={`${IMAGE_URL}w500${crew.profile_path}`}
                />
                
                }

              </React.Fragment>
            ))}
        </Row>}

        <Comments CommentLists={CommentLists} postId={movieId} refreshFunction={updateComment}/>


      </div>
    </div>
  );
}

export default MovieDetailPage;
