import React, {useEffect, useState, useMemo} from "react";
import axios from 'axios';
import { useSelector } from 'react-redux';


function Favorite(props) {
    const user = useSelector(state => state.user)


    const [FavoriteNumber, setFavoriteNumber] = useState(0);
    const [Favorited, setFavorited] = useState(false);
    const variable = useMemo(() => ({
        userFrom: props.userFrom,
        movieId:  props.movieId,
        movieTitle:  props.movieInfo.original_title,
        movieImage:  props.movieInfo.backdrop_path,
        movieRunTime: props.movieInfo.runtime
    }), [props.userFrom, props.movieId, props.movieInfo]);
    useEffect(() => {

        axios.post('/api/favorite/favoriteNumber', variable)
            .then(response => {
                if(response.data.success){
                    setFavoriteNumber(response.data.favoriteNumber)
                }
            })


        axios.post('/api/favorite/favorited', variable)
            .then(response => {
                if(response.data.success){
                    setFavorited(response.data.favorited)
                }
            })

        
        
    }, [variable]);


    const onClickFavorite = () =>{
        if (user.userData && !user.userData.isAuth) {
            return alert('Please Log in first');
        }
        if(Favorited){
            // Si deja ajoute
            axios.post('/api/favorite/removeFromFavorite', variable)
            .then(response=>{
                if(response.data.success){
                    setFavoriteNumber(FavoriteNumber - 1)
                    setFavorited(!Favorited) 
                }else{
                    alert('Failed to remove from favorite')
                }
            })

        }else{
            // Si pas ajoute
            axios.post('/api/favorite/addToFavorite', variable)
                .then(response=>{
                    if(response.data.success){
                        setFavoriteNumber(FavoriteNumber + 1)
                        setFavorited(!Favorited)        
                    }else{
                        alert('Failed to add to favorites')
                    }
                })
        }
    }
    
  return (
    <div>
      <button onClick={onClickFavorite}> {Favorited ? "Remove from Favorite" : "Add to Favorite"} {FavoriteNumber}</button>
    </div>
  );
}

export default Favorite;
