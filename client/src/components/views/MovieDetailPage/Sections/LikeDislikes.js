import { Icon, Tooltip } from 'antd'
import React, { useEffect, useState, useMemo } from 'react'
import axios from 'axios';
import { useSelector } from 'react-redux';


function LikeDislikes(props) {
    const user = useSelector(state => state.user)


    const [Likes, setlikes] = useState(0)
    const [Dislikes, setDislikes] = useState(0)
    const [LikeAction, setlikeAction] = useState(null)
    const [DislikeAction, setDislikeAction] = useState(null)


    const variable = useMemo(() => {
        if (props.movie) {
            return { movieId: props.movieId, userId: props.userId };
        } else {
            return { commentId: props.commentId, userId: props.userId };
        }
    }, [props.movie, props.movieId, props.userId, props.commentId]);


    useEffect(() =>{

        axios.post('/api/like/getLikes', variable)
        .then(response => {
            if(response.data.success){
                // Cb il y a de commentaire ou de like
                setlikes(response.data.likes.length)
                
                // Si déjà cliqué sur le boutton de like ou pas
                response.data.likes.map(like => {
                    if(like.userId === props.userId){
                        setlikeAction('liked')
                    }
                    return null;
                })
            }else{
                alert('Failed to get likes')
            }
        })

        axios.post('/api/like/getDisLikes', variable)
        .then(response => {
            if(response.data.success){
                // Cb il y a de commentaire ou de like
                setDislikes(response.data.dislikes.length)
                
                // Si déjà cliqué sur le boutton de like ou pas
                response.data.dislikes.map(dislike => {
                    if(dislike.userId === props.userId){
                        setDislikeAction('disliked')
                    }
                    return null;
                })
            }else{
                alert('Failed to get dislikes')
            }
        })

    }, [props.userId, variable])

    const onLike = () =>{
        if (user.userData && !user.userData.isAuth) {
            return alert('Please Log in first');
        }
        if(LikeAction === null){
            axios.post('/api/like/upLike', variable)
            .then(response =>{
                if(response.data.success){
                    setlikes(Likes + 1)
                    setlikeAction('liked')

                    // Si le boutton de de dislike est déjà cliqué
                    if(DislikeAction !== null){
                        setDislikeAction(null)
                        setDislikes(Dislikes - 1)
                    }

                }
                else {
                    alert('Failed to increse the like')
                }
            })
        }else{ 
            axios.post('/api/like/unlike', variable)
            .then(response =>{
                if(response.data.success){
                    setlikes(Likes - 1)
                    setlikeAction(null)

                }
                else {
                    alert('Failed to decrese the like')
                }
            })
            
        }
    }

    const onDislike = () =>{
        if (user.userData && !user.userData.isAuth) {
            return alert('Please Log in first');
        }
        if(DislikeAction !== null){
            axios.post('/api/like/unDislike', variable)
            .then(response =>{
                if(response.data.success){
                    setDislikes(Dislikes - 1)
                    setDislikeAction(null)
                }else{
                    alert('Failed to decrease dislike')
                }
            })
        }else{
            axios.post('/api/like/upDislike', variable)
            .then(response =>{
                if(response.data.success){
                    setDislikes(Dislikes + 1)
                    setDislikeAction('disliked')

                    // Si le boutton de de dislike est déjà cliqué
                    if(LikeAction !== null){
                        setlikeAction(null)
                        setlikes(Likes - 1)   
                    }

                }else{
                    alert('Failed to increase dislike')
                }
            })
        }
    }


  return (
    <React.Fragment>
        <span key={"comment-basic-like"}>
            <Tooltip title="Like">
                <Icon type='like'
                    theme={LikeAction === 'liked' ? 'filled' : 'outlined'}
                    onClick={onLike} />
            </Tooltip>
            <span style={{paddingLeft:'8px', cursor:'auto'}}>{Likes}</span>
        </span>&nbsp;&nbsp;
        <span key="comment-basic-dislike">
            <Tooltip title="Dislike">
                <Icon
                    type="dislike"
                    theme={DislikeAction === 'disliked' ? 'filled': 'outlined'}
                    onClick={onDislike}
                    />
            </Tooltip>
            <span style={{paddingLeft: '8px', cursor: 'auto'}}>{Dislikes}</span>
        </span>
    </React.Fragment>
  )
}

export default LikeDislikes 