import React, { useState } from 'react';
import { Comment, Avatar, Button, Input } from 'antd';
import axios from 'axios';
import { useSelector } from 'react-redux';
import LikeDislikes from './LikeDislikes';
import { EditOutlined } from '@ant-design/icons';

const { TextArea } = Input;

function SingleComment(props) {

    const user = useSelector(state => state.user);
    const isAdmin = user.userData && user.userData.role === 1;

    const [CommentValue, setCommentValue] = useState("");
    const [OpenReply, setOpenReply] = useState(false);
    const [isEditing, setIsEditing] = useState(false);
    const [editedCommentValue, setEditedCommentValue] = useState(props.comment.content);

    const openReply = () => {
        setOpenReply(!OpenReply);
    };

    const toggleEdit = () => {
        setIsEditing(!isEditing);
    };

    const onSubmit = e => {
        e.preventDefault();
        const variables = {
            writer: user.userData._id,
            postId: props.postId,
            responseTo: props.comment._id,
            content: CommentValue
        };

        axios.post('/api/comment/saveComment', variables)
            .then(response => {
                if (response.data.success) {
                    setCommentValue("");
                    setOpenReply(!OpenReply);
                    props.refreshFunction(response.data.result);
                } else {
                    alert('Failed to save comment');
                }
            });
    };

    const handleDelete = () => {
        const commentId = props.comment._id;
    
        axios.delete(`/api/comment/deleteComment/${commentId}`)
            .then(response => {
                if (response.data.success) {
                    props.refreshFunction(commentId, "delete");
                } else {
                    alert('Failed to delete comment');
                }
            });
    };

    const handleEditSubmit = () => {
        const commentId = props.comment._id;

        axios.put(`/api/comment/updateComment/${commentId}`, { content: editedCommentValue })
            .then(response => {
                if (response.data.success) {
                    setIsEditing(false);
                    props.refreshFunction(response.data.result, "update");
                } else {
                    alert('Failed to save comment');
                }
            });
    };

    const action = [
        <LikeDislikes comment commentId={props.comment._id} userId={localStorage.getItem('userId')} />,
        <span onClick={openReply} key="comment-basic-reply-to">Reply to </span>,
        isAdmin && (
            <>
                <span onClick={handleDelete} key="comment-basic-delete">Delete</span>
                {isEditing ? (
                    <Button type="primary" onClick={handleEditSubmit}>Submit</Button>
                ) : (
                    <EditOutlined onClick={toggleEdit} key="comment-basic-edit" style={{ marginLeft: '8px', cursor: 'pointer' }} />
                )}
            </>
        )
    ];
    
    const handleChange = e => {
        if (user.userData && !user.userData.isAuth) {
            return alert('Please Log in first');
        }
        setCommentValue(e.currentTarget.value);
    };

    const handleEditChange = e => {
        setEditedCommentValue(e.currentTarget.value);
    };

    const commentContent = isEditing ? (
        <TextArea
            style={{ width: '100%', borderRadius: '5px' }}
            onChange={handleEditChange}
            value={editedCommentValue}
            placeholder="Edit your comment"
        />
    ) : (
        <p>{props.comment.content}</p>
    );

    return (
        <div>
            {props.comment && props.comment.writer && props.comment.writer.name &&
                <Comment
                    actions={action}
                    author={props.comment.writer.name}
                    avatar={<Avatar src={props.comment.writer.image} alt="image" />}
                    content={commentContent}
                />}
            {OpenReply && (
                <form style={{ display: 'flex' }} onSubmit={onSubmit}>
                    <TextArea
                        style={{ width: '100%', borderRadius: '5px' }}
                        onChange={handleChange}
                        value={CommentValue}
                        placeholder="write some comments"
                    />
                    <br />
                    <Button style={{ width: '20%', height: '52px' }} onClick={onSubmit}>Submit</Button>
                </form>
            )}
        </div>
    );
}

export default SingleComment;
