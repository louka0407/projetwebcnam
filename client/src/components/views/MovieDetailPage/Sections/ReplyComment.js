import React, { useState } from "react";
import SingleComment from "./SingleComment";

function ReplyComment(props) {

    const [OpenReplyComments, setOpenReplyComments] = useState(false)

    const renderReplyComment = (parentCommentId) => {
        return (
            props.CommentLists && props.CommentLists.map((comment, index) => (
                <React.Fragment key={index}>
                    {comment.responseTo === parentCommentId && (
                        <div style={{ marginLeft: '50px', width: '80%' }}>
                            <SingleComment
                                comment={comment}
                                postId={props.postId}
                                refreshFunction={props.refreshFunction}
                            />
                            <ReplyComment CommentLists={props.CommentLists} parentCommentId={comment._id} postId={props.postId} refreshFunction={props.refreshFunction} />
                        </div>
                    )}
                </React.Fragment>
            ))
        );
    }

    const handleChange = () => {
        setOpenReplyComments(!OpenReplyComments)
    }

    return (
        <div>
            {props.CommentLists && props.CommentLists.filter(comment => comment.responseTo === props.parentCommentId).length > 0 && (
                <p style={{ fontSize: "14px", margin: 0, color: "gray" }} onClick={handleChange}>
                    View {props.CommentLists.filter(comment => comment.responseTo === props.parentCommentId).length} more comment(s)
                </p>
            )}

            {OpenReplyComments && 
                renderReplyComment(props.parentCommentId)
            }
        </div>
    );
}

export default ReplyComment;
