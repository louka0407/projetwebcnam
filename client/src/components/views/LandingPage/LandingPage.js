import React, { useEffect, useState, useCallback } from "react";
import { API_KEY, API_URL, IMAGE_URL } from "../../Config";
import { Typography, Row, Input } from "antd";
import MainImage from "./Sections/MainImage";
import GridCard from "./Sections/GridCard";

const { Title } = Typography;
const { Search } = Input;

function LandingPage() {
  const [Movies, setMovies] = useState([]);
  const [CurrentPage, setCurrentPage] = useState(1);
  const [SearchTerm, setSearchTerm] = useState(""); // État pour la barre de recherche

  const fetchMovies = useCallback((path) => {
    fetch(path)
      .then((response) => response.json())
      .then((response) => {
        console.log(response);
        setMovies((prevMovies) => [...prevMovies, ...response.results]);
        setCurrentPage(response.page);
      });
  }, []);

  useEffect(() => {
    const endpoint = `${API_URL}movie/now_playing?api_key=${API_KEY}&language=en-US&page=1`;
    fetchMovies(endpoint);
  }, [fetchMovies]);

  const handleClick = () => {
    let endpoint = `${API_URL}movie/now_playing?api_key=${API_KEY}&language=en-US&page=${CurrentPage + 1}`;
    fetchMovies(endpoint);
  };

  const handleSearchChange = (event) => {
    setSearchTerm(event.target.value);
  };

  const handleSearch = (value) => {
    if (value) {
      const searchEndpoint = `${API_URL}search/movie?api_key=${API_KEY}&language=en-US&query=${value}`;
      fetch(searchEndpoint)
        .then((response) => response.json())
        .then((response) => {
          setMovies(response.results);
        });
    } else {
      const endpoint = `${API_URL}movie/now_playing?api_key=${API_KEY}&language=en-US&page=1`;
      fetchMovies(endpoint);
    }
  };

  return (
    <div style={{ width: "100%", margin: 0 }}>
      {/* Barre de recherche */}
      <div style={{ width: "85%", margin: "1rem auto" }}>
        <Search
          placeholder="Search by movie name"
          onChange={handleSearchChange}
          value={SearchTerm}
          onSearch={handleSearch}//prend en parametre la value
          enterButton
        />
      </div>

      {/*------------ Movie Main Image ------------*/}
      {Movies[0] && (
        <MainImage
          image={`${IMAGE_URL}w1280${
            Movies[0].backdrop_path && Movies[0].backdrop_path
          }`}
          title={Movies[0].original_title}
          text={Movies[0].overview}
        />
      )}

      {/* Body */}
      <div style={{ width: "85%", margin: "1rem auto" }}>
        <Title level={2}>Movies by latest</Title>
        <hr />

        {/*----------- Grid cards -------------*/}
        <Row gutter={[16, 16]}>
          {Movies &&
            Movies.map((movie, index) => (
              <React.Fragment key={index}>
                <GridCard
                  image={
                    movie.poster_path && `${IMAGE_URL}w500${movie.poster_path}`
                  }
                  movieId={movie.id}
                />
              </React.Fragment>
            ))}
        </Row>

        {/* Boutton Load More */}
        <br />
        <div style={{ display: "flex", justifyContent: "center" }}>
          <button onClick={handleClick}>Load More</button>
        </div>
      </div>
    </div>
  );
}

export default LandingPage;
