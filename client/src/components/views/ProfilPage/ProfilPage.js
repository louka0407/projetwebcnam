import React, { useState, useEffect } from 'react';
import { Card, Avatar, Form, Input, Button, message, Descriptions, Badge } from 'antd';
import { useSelector } from 'react-redux';
import { UserOutlined } from '@ant-design/icons';
import axios from 'axios';
import { Formik } from 'formik';
import * as Yup from 'yup';
import UserList from './UserList';

const { Meta } = Card;

function ProfilPage() {
  const userData = useSelector(state => state.user.userData);
  const [user, setUser] = useState(userData); // Initialiser avec les données utilisateur s'il y en a, sinon null
  const [loading, setLoading] = useState(false);

  // Mettre à jour les données utilisateur locales lorsque les données utilisateur Redux changent
  useEffect(() => {
    setUser(userData);
  }, [userData]);

  // Vérifier si les données utilisateur sont chargées
  if (!user) {
    return <div>Loading...</div>;
  }

  return (
    <div style={{ display: 'flex', flexDirection: 'column', alignItems: 'center', height: '100vh', backgroundColor: '#f0f2f5' }}>
      <div style={{ marginBottom: '20px' }}>
        <Card
          style={{ width: 600, borderRadius: 10, boxShadow: '0 4px 8px rgba(0, 0, 0, 0.2)' }}
        >
          <Meta
            avatar={<Avatar src={user.image || <UserOutlined />} size={100} alt={user.name}/>}
            title={<h2>{user.name}</h2>}
            description={
              <Formik
                initialValues={{
                  email: user?.email || '',
                  name: user?.name || '',
                  password: '',
                }}
                validationSchema={Yup.object().shape({
                  email: Yup.string().email('Invalid email').required('Email is required'),
                  name: Yup.string().required('Name is required'),
                  password: Yup.string().min(6, 'Password must be at least 6 characters long').required('Password is required'),
                })}
                onSubmit={async (values, { setSubmitting }) => {
                  setLoading(true);
                  try {
                    const response = await axios.post('/api/users/update', values);
                    if (response.data.success) {
                      message.success('Information updated successfully');
                      setUser({ ...user, ...values }); // Mettre à jour les données utilisateur locales
                    } else {
                      message.error('Failed to update information');
                    }
                  } catch (error) {
                    message.error('Error updating information');
                  } finally {
                    setLoading(false);
                    setSubmitting(false);
                  }
                }}
              >
                {({
                  values,
                  errors,
                  touched,
                  handleChange,
                  handleBlur,
                  handleSubmit,
                  isSubmitting,
                }) => (
                  <Form layout="vertical" onSubmit={handleSubmit}>
                    <Form.Item
                      label="Email"
                      validateStatus={errors.email && touched.email ? 'error' : ''}
                      help={errors.email && touched.email ? errors.email : ''}
                    >
                      <Input
                        name="email"
                        type="text"
                        value={values.email}
                        onChange={handleChange}
                        onBlur={handleBlur}
                      />
                    </Form.Item>
                    <Form.Item
                      label="Name"
                      validateStatus={errors.name && touched.name ? 'error' : ''}
                      help={errors.name && touched.name ? errors.name : ''}
                    >
                      <Input
                        name="name"
                        type="text"
                        value={values.name}
                        onChange={handleChange}
                        onBlur={handleBlur}
                      />
                    </Form.Item>
                    <Form.Item
                      label="Password"
                      validateStatus={errors.password && touched.password ? 'error' : ''}
                      help={errors.password && touched.password ? errors.password : ''}
                    >
                      <Input.Password
                        name="password"
                        value={values.password}
                        onChange={handleChange}
                        onBlur={handleBlur}
                      />
                    </Form.Item>
                    <Form.Item>
                      <Button type="primary" htmlType="submit" loading={loading} disabled={isSubmitting}>
                        Update
                      </Button>
                    </Form.Item>
                  </Form>
                )}
              </Formik>
            }
          />
          <Descriptions column={1}>
            <Descriptions.Item label="Rôle">
              <Badge status={user.role === 0 ? "success" : "error"} text={user.role === 0 ? "Utilisateur" : "Administrateur"} />
            </Descriptions.Item>
          </Descriptions>
        </Card>
      </div>
      <div>
        <UserList />
      </div>
    </div>
  );
}

export default ProfilPage;
