# Movie site

This project, carried out as part of CNAM, is a React-based website dedicated to searching and managing movies. The application allows users to search for movies, view detailed information about each movie, and add them to their favorites. Additionally, each movie has a comment section where users can share their opinions and impressions. The site also includes a like system, allowing users to express their appreciation for the movies they enjoy.

## Prerequisites

Before you begin, ensure you have the following software installed:

- [Node.js](https://nodejs.org/) (version 16 recommended)
- [npm](https://www.npmjs.com/) (managed by Node.js installation)
- [MongoDB](https://www.mongodb.com/)

## Installation

1. Clone the repository:

```bash
git clone https://gitlab.com/louka0407/projetwebcnam.git

```
2. Navigate to the project directory:
:
```bash
cd projet
```
3.Install server and client dependencies:
```bash
npm install
cd client && npm install
```

## Configuration
1. Create a dev.js file in the server/src/config folder with the following content for MongoDB database connection:
```bash
module.exports = {
    mongoURI: 'mongodb+srv://<username>:<password>@<your-db-url>?retryWrites=true&w=majority'
}

```
Replace username, password, and your-db-url with your MongoDB database information.

2. Create a .env file in the server folder with configurations for email sending:
```bash
BASE_URL="http://127.0.0.1:3000/"
HOST="smtp.gmail.com"
SERVICE="gmail"
EMAIL_PORT=587
SECURE=true
USER_MAIL="your-email@gmail.com"
PASS="your-password"

```
Make sure to replace your-email@gmail.com and your-password with your Gmail email address and password.

## Exécution
1. Ensure you have activated Node.js version 16 using nvm:
```bash
nvm use 16
````
2. Start the server and client in development mode:
```bash
npm run dev
````
3. The site will be accessible at the following address: http://localhost:3000/