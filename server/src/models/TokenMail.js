const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const tokenMailSchema = new Schema({
    userId:{
        type: Schema.Types.ObjectId,
        required: true,
        ref:'User',
        unique: true,
    },
    tokenMail:{
        type: String,
        required: true,
    },
    createdAt:{
        type: Date,
        default:Date.now(),
        expires:3600 // 1h
    }
});

module.exports = mongoose.model('TokenMail', tokenMailSchema);