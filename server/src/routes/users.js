const express = require('express');
const router = express.Router();
const { User } = require("../models/User");
const { auth } = require("../middleware/auth");
const TokenMail = require("../models/TokenMail")
const sendEmail = require("../utils/sendEmail");
const crypto = require("crypto");
const dotenv = require('dotenv');
const bcrypt = require('bcrypt');
const saltRounds = 10;

dotenv.config()


//=================================
//             User
//=================================

router.get("/auth", auth, (req, res) => {
    res.status(200).json({
        _id: req.user._id,
        isAdmin: req.user.role === 0 ? false : true,
        isAuth: true,
        email: req.user.email,
        name: req.user.name,
        lastname: req.user.lastname,
        role: req.user.role,
        image: req.user.image,
    });
});

router.post("/register", async (req, res) => { 

    const user = new User(req.body);

    try {
        const doc = await user.save();

        const tokenMail = await new TokenMail({
            userId: user._id,
            tokenMail: crypto.randomBytes(32).toString("hex")
        }).save();
        const url = `${process.env.BASE_URL}users/${user._id}/verify/${tokenMail.tokenMail}`;
        await sendEmail(user.email, "Verify Email", url);

        return res.status(200).json({
            success: true,
            message: "An Email sent to your account please verify"
        });
    } catch (err) {
        return res.json({ success: false, err });
    }
});

router.post("/login", async (req, res) => {
    try {
        const user = await User.findOne({ email: req.body.email });

        if (!user)
            return res.json({
                loginSuccess: false,
                message: "Auth failed, email not found"
            });

        if (!user.verified) {
            let tokenMail = await TokenMail.findOne({ userId: user._id });
            if (!tokenMail) {
                tokenMail = await new TokenMail({
                    userId: user._id,
                    tokenMail: crypto.randomBytes(32).toString("hex")
                }).save();
                const url = `${process.env.BASE_URL}users/${user._id}/verify/${tokenMail.tokenMail}`;
                await sendEmail(user.email, "Verify Email", url);
            }
            return res.status(400).send({ message: 'An Email sent to your account please verify' });
        }

        user.comparePassword(req.body.password, async (err, isMatch) => { 
            if (!isMatch)
                return res.json({ loginSuccess: false, message: "Wrong password" });

            user.generateToken(async (err, user) => { 
                if (err) return res.status(400).send(err);
                res.cookie("w_authExp", user.tokenExp);
                res
                    .cookie("w_auth", user.token)
                    .status(200)
                    .json({
                        loginSuccess: true, userId: user._id
                    });
            });
        });
    } catch (err) {
        return res.json({ loginSuccess: false, message: err.message });
    }
});

router.get("/logout", auth, (req, res) => {
    User.findOneAndUpdate({ _id: req.user._id }, { token: "", tokenExp: "" }, (err, doc) => {
        if (err) return res.json({ success: false, err });
        return res.status(200).send({
            success: true
        });
    });
});

router.get("/:id/verify/:tokenMail/", async(req,res) =>{
    try {
        const user = await User.findOne({_id : req.params.id});
        if(!user) return res.status(400).send({message: "Invalid link"});

        const tokenMail = await TokenMail.findOne({
            userId: user._id,
            tokenMail: req.params.tokenMail
        })
        if(!tokenMail) return res.status(400).send({message: "Invalid link"});

        await User.updateOne({_id: user._id}, { verified: true });

        await tokenMail.remove();

        res.status(200).send({message: "Email verified successfully"})
    } catch (error) {
        res.status(500).send({message:"Internal Server Error"});
    }
})

router.post("/update", auth, async (req, res) => {
    try {
      const userId = req.user._id;
      const { email, name, password } = req.body;

      const hashedPassword = await bcrypt.hash(password, saltRounds);

      const updatedUserData = { email, name, password: hashedPassword };
  
      await User.findByIdAndUpdate(userId, updatedUserData);
  
      res.status(200).json({ success: true });
    } catch (error) {
      console.error(error);
      res.status(500).json({ success: false, message: "An error occurred while updating the user" });
    }
});

// Route pour la modification des utilisateurs
router.post("/updateUser", auth, async (req, res) => {
    try {
        console.log("Update request received for user:", req.body);

        const { userId, email, name, role } = req.body;

        const user = await User.findById(userId);
        if (!user) {
            return res.status(404).json({ success: false, message: "User not found" });
        }

        // Vérifier si l'email est déjà utilisé par un autre utilisateur
        const existingUser = await User.findOne({ email });
        if (existingUser && existingUser._id.toString() !== userId) {
            return res.status(400).json({ success: false, message: "Email already in use" });
        }

        await User.findByIdAndUpdate(userId, { email, name, role });

        res.status(200).json({ success: true });
    } catch (error) {
        console.error(error);
        res.status(500).json({ success: false, message: "An error occurred while updating the user" });
    }
});

// Récupérer tous les utilisateurs depuis la base de données
router.get("/", auth, async (req, res) => {
    try {
      const users = await User.find();
  
      res.status(200).json(users);
    } catch (error) {
      console.error('Erreur lors de la récupération des utilisateurs :', error);
      res.status(500).json({ message: 'Erreur lors de la récupération des utilisateurs' });
    }
  });


// Route pour la suppression des utilisateurs
router.post("/deleteUser", auth, async (req, res) => {
    try {
        const { userId } = req.body;

        const user = await User.findById(userId);
        if (!user) {
            return res.status(404).json({ success: false, message: "User not found" });
        }

        await User.findByIdAndRemove(userId);

        res.status(200).json({ success: true });
    } catch (error) {
        console.error(error);
        res.status(500).json({ success: false, message: "An error occurred while deleting the user" });
    }
});



module.exports = router;
