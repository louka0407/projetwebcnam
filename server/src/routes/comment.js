const express = require('express');
const router = express.Router();
const { Comment } = require("../models/Comment");

//=================================
//             Comment
//=================================


router.post("/saveComment", (req, res) => {

    const comment = new Comment(req.body)
    comment.save((err, comment) =>{
        if(err) return res.json({success: false, err})
        // permet de recuperer toutes les donnees associe au user
        Comment.find({'_id': comment._id})
        .populate('writer')
        .exec((err, result) =>{
            if(err) return res.json({success:false, err})
            return res.status(200).json({success:true, result})
        })
    })
});

router.post("/getComments", (req, res) => {

    Comment.find({"postId":req.body.movieId})
    .populate('writer')
    .exec((err, comments)=>{
        if(err) return res.status(400).send(err)
        res.status(200).json({success:true, comments})
    })
});

router.delete("/deleteComment/:commentId", (req, res) => {
    Comment.findByIdAndRemove(req.params.commentId)
        .exec((err, deletedComment) => {
            if (err) return res.status(400).send(err);
            res.status(200).json({ success: true, result: deletedComment });
        });
});

router.put("/updateComment/:commentId", (req, res) => {
    const { commentId } = req.params;
    const { content } = req.body;

    Comment.findByIdAndUpdate(commentId, { content }, { new: true })
    .populate('writer')
        .exec((err, updatedComment) => {
            if (err) return res.status(400).send(err);
            res.status(200).json({ success: true, result: updatedComment });
        });
});







module.exports = router;