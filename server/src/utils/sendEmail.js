const nodemailer = require("nodemailer");
const dotenv = require('dotenv');

dotenv.config()

module.exports = async (email,subject, text) =>{
    try {
        const transporter = nodemailer.createTransport({
            //host:process.env.HOST,
            service: process.env.SERVICE,
            //port: Number(process.env.EMAIL_PORT),
            auth:{
                user: process.env.USER_MAIL,
                pass: process.env.PASS
            },
        })

        await transporter.sendMail({
            from: process.env.USER_MAIL,
            to: email,
            subject: subject,
            text: text
        });
        console.log("Email sent Successfully");
        
    } catch (error) {
        console.log("Email not sent");
        console.log(error)
    }
}